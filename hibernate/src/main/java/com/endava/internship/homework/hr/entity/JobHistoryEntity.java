package com.endava.internship.homework.hr.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "job_history")
public class JobHistoryEntity {

    @EmbeddedId
    JobHistoryId jobHistoryId;

    @Column(name = "end_date")
    LocalDateTime endDate;

    @OneToOne
    @JoinColumn(name = "job_id",insertable = false, updatable = false)
    JobsEntity job;

    @OneToOne
    @JoinColumn(name = "department_id",insertable = false, updatable = false)
    DepartmentsEntity department;
}
