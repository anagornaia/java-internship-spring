package com.endava.internship.homework.hr.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "jobs")
public class JobsEntity {

    @Id
    @Column(name = "job_id")
    String jobId;

    @Column(name = "job_title")
    String jobTitle;

    @Column(name = "min_salary")
    Long minSalary;

    @Column(name = "max_salary")
    Long maxSalary;
}
