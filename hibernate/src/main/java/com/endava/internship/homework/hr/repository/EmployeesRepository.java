package com.endava.internship.homework.hr.repository;

import com.endava.internship.homework.hr.entity.EmployeesEntity;
import org.springframework.data.repository.Repository;

public interface EmployeesRepository extends Repository<EmployeesEntity, Long> {
    EmployeesEntity getEmployeesEntityByEmployeeId(Long id);
}
