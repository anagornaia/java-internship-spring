package com.endava.internship.homework.hr.entity;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Embeddable
public class JobHistoryId implements Serializable {

    @Column(name = "department_id")
    Long employeeId;

    @Column(name = "start_date")
    LocalDateTime startDate;
}
