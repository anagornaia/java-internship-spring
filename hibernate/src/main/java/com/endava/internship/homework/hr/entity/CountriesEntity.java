package com.endava.internship.homework.hr.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "countries")
public class CountriesEntity {

    @Id
    @Column(name = "country_id", columnDefinition = "CHAR")
    String countryId;

    @Column(name = "country_name")
    String countryName;

    @OneToOne
    @JoinColumn(name = "region_id")
    RegionsEntity region;
}

