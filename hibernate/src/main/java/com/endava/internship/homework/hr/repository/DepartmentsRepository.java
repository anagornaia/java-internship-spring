package com.endava.internship.homework.hr.repository;

import com.endava.internship.homework.hr.entity.DepartmentsEntity;
import org.springframework.data.repository.Repository;

import java.util.List;


public interface DepartmentsRepository extends Repository<DepartmentsEntity, Long> {

    List<DepartmentsEntity> getAllByDepartmentID(Long id);
}
