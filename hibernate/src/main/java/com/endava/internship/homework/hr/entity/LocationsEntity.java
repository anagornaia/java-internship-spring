package com.endava.internship.homework.hr.entity;

import lombok.Data;
import javax.persistence.Id;

import javax.persistence.*;

@Data
@Entity
@Table(name = "locations")
public class LocationsEntity {

    @Id
    @Column(name = "location_id")
    @SequenceGenerator(name = "location_id_generator", sequenceName = "locations_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "location_id_generator")
    Long locationId;

    @Column(name = "street_address")
    String streetAddress;

    @Column(name = "postal_code")
    String postalCode;

    String city;

    @Column(name = "state_province")
    String stateProvince;

    @OneToOne
    @JoinColumn(name = "country_id")
    CountriesEntity country;

}
