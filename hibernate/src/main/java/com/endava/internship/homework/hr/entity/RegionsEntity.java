package com.endava.internship.homework.hr.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "regions")
public class RegionsEntity {

    @Id
    @Column(name = "region_id")
    Long regionId;

    @Column(name = "region_name")
    String regionName;
}
