package com.endava.internship.homework.hr.entity;

import lombok.Data;
import javax.persistence.Id;


import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "departments")
public class DepartmentsEntity {

    @Id
    @Column(name = "department_id")
    @SequenceGenerator(name = "department_id_generator", sequenceName = "departments_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "department_id_generator")
    Long departmentID;

    String departmentName;

    @OneToOne
    @JoinColumn(name = "manager_id")
    EmployeesEntity manager;

    @OneToOne
    @JoinColumn(name = "location_id")
    LocationsEntity location;

    @OneToMany(mappedBy = "department")
    Set<EmployeesEntity> employees;
}
