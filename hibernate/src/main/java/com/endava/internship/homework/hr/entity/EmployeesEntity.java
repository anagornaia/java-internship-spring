package com.endava.internship.homework.hr.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Id;


import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "employees")
public class EmployeesEntity {

    @Id
    @Column(name = "employee_id")
    @SequenceGenerator(name = "employee_id_generator", sequenceName = "employees_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_id_generator")
    Long employeeId;

    @Column(name = "first_name")
    String firstName;

    @Column(name = "last_name")
    String lastName;

    String email;

    @Column(name = "phone_number")
    String phoneNumber;

    @Column(name = "hire_date")
    LocalDateTime hireDate;

    @ManyToOne
    @JoinColumn(name = "job_id")
    JobsEntity job;

    BigDecimal salary;

    @Column(name = "commission_pct")
    BigDecimal commissionPct;

    @ManyToOne
    @JoinColumn(name = "manager_id", referencedColumnName = "employee_id")
    EmployeesEntity managerId;

    @ManyToOne
    @JoinColumn(name = "department_id")
    DepartmentsEntity department;
}
