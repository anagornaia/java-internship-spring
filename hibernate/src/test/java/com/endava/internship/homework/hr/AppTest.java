package com.endava.internship.homework.hr;

import com.endava.internship.homework.hr.entity.DepartmentsEntity;
import com.endava.internship.homework.hr.entity.EmployeesEntity;
import com.endava.internship.homework.hr.repository.DepartmentsRepository;
import com.endava.internship.homework.hr.repository.EmployeesRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class AppTest {

    @Autowired
    EmployeesRepository employeesRepository;

    @Autowired
    DepartmentsRepository departmentsRepository;

    @Test
    public void test() {
        EmployeesEntity result = employeesRepository.getEmployeesEntityByEmployeeId(100L);
//        log.info("Query result {}", result);

        List<DepartmentsEntity> resultList = departmentsRepository.getAllByDepartmentID(60L);
        log.info("Query result list {}", resultList.get(0).getEmployees().size());
    }
}